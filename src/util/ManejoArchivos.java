package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author randall
 */
public class ManejoArchivos {

    /**
     * Lee el contenido de un archivo
     *
     * @param ruta String ruta del archivo
     * @return String contenido del archivo
     */
    public String leer(String ruta) {
        String datos = "";
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {//Lo que intento hacer 
            archivo = new File(ruta); // Busca

            if (!archivo.exists()) { //Si el archivo no existe
                archivo.getParentFile().mkdirs(); //Crea los directorios/carpetas
                archivo.createNewFile(); //Crea el archivo
            }

            fr = new FileReader(archivo); // Abre
            br = new BufferedReader(fr); // Leer

            String linea;
            while ((linea = br.readLine()) != null) {
                datos += linea + "\n";
            }
            //Absoluta: c:/Users/hanzel/Documents/NetBeans Projects/datos/prueba.txt
            //Relativa: prueba.txt

        } catch (Exception e) {
            //Lo que hago si se cae
            //TODO: Manejar correctamente el error
            e.printStackTrace();
        } finally {
            //Obligatorio ejecutarlo independientemente si cae o no  
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return datos;
    }

    /**
     * Escribe los datos al final del archivo
     *
     * @param ruta String ruta del archivo
     * @param datos String contenido a agregar
     */
    public void escribir(String ruta, String datos) {
        escribir(ruta, datos, true);
    }

    /**
     * Escribe datos en un archivo
     *
     * @param ruta String ruta del archivo
     * @param datos String contenido a escribir
     * @param agregar boolean, true: agrega el contenido al final del archivo
     * false: sobreescribe el contenido del archivo
     */
    public void escribir(String ruta, String datos, boolean agregar) {
        File archivo = null;
        FileWriter fw = null;
        PrintWriter pw = null;
        try {
            archivo = new File(ruta); // Busca el archivo 
            if (!archivo.exists()) { //Si el archivo no existe
                archivo.getParentFile().mkdirs(); //Crea los directorios/carpetas             
            }
            fw = new FileWriter(ruta, agregar); //Si el archivo no existe lo crea, pero no las carpetas
            pw = new PrintWriter(fw);
            pw.println(datos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
