package randallarceagenda;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import util.ManejoArchivos;
import util.Util;

/**
 *
 * @author Randall
 */
public class LogicaProyecto {

    private ManejoArchivos ma;
// Rutas para guardar texto.
    private static final String RUTA_PERS = "datos/persona.txt";
    private static final String RUTA_CONT = "datos/contactos.txt";
    private static final String RUTA_ACTI = "datos/actividades.txt";
    private static final String RUTA_FEST = "datos/diaFestivos.txt";
    private String usuarioLog;
    private Date ho;
    private String contacto = "";

    public LogicaProyecto() {
        ma = new ManejoArchivos();
    }
// Metodos para registrar

    public void registrarPersona(PersonasProyecto nueva) {
        ma.escribir(RUTA_PERS, nueva.getInfo());
    }

    public void registrarContacto(Contactos nueva) {
        ma.escribir(RUTA_CONT, nueva.getInfo());
    }

    public void registrarAtividad(Actividad nueva) {
        ma.escribir(RUTA_ACTI, nueva.getInfo());
    }

    public void registrarDiaFestivo(DiaFestivo nueva) {
        ma.escribir(RUTA_FEST, nueva.getInfo());
    }

    // Metodos para mostrar
    public void mostrarPersona(PersonasProyecto nueva) {
        ma.escribir(RUTA_PERS, nueva.getInfo());
    }

    public String festivo(String fecha) {
        String f = "";
        DiaFestivo[] dia = consultarFestivos();
        for (int i = 0; i < dia.length; i++) {
            DiaFestivo df = dia[i];
            if (fecha.equals(df.getFechaString())) {
                f = "Día Festivo";
            }
        }

        return f;
    }

    private int calcEdad(String fecha) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecNan = LocalDate.parse(fecha, dtf);
        LocalDate hoy = LocalDate.now();

        Period periodo = Period.between(fecNan, hoy);
//        System.out.printf"%d años, %d meses, %d días",
//                periodo.getYears(), periodo.getMonths(), periodo.getDays());

        return periodo.getYears();
    }

    public Date calcHora(int n) {
        Date fecha = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); //tuFechaBase es un Date;
        calendar.add(Calendar.DAY_OF_WEEK, n); //minutosASumar es int.
        //lo que más quieras sumar
        Date fechaSalida = calendar.getTime(); //Y ya tienes la fecha sumada.
//        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("hh:mm a");
//        LocalTime horaNan = LocalTime.parse(hora, dtf1);
//        horaNan.getMinute();
//        LocalTime h = LocalTime.now();
//        h.getMinute();

        return fechaSalida;
    }

    public int edad(String nombre) {
        int f = 0;
        Contactos[] con = consultarContactoArr(nombre, usuarioLog);
        for (int i = 0; i < con.length; i++) {
            Contactos c = con[i];
            if (nombre.equals(c.getNombre())) {
                int edad = calcEdad(c.getFechaNacimientoString());
                f = edad;
                break;
            }
        }

        return f;
    }

    // Método para la notificaciones de la agenda
    public Actividad[] notificacionesArr(Date date) {

        int con = 0;
        Actividad[] a = consultarActividadesUser();
        if (a.length == 0) {
            menuPrincipal();
        }
        if (a[0] == null) {
            menuPrincipal();
        } else {
            for (int i = 0; i < a.length; i++) {
                if (a[i] != null) {
                    con++;
                }
            }
            Actividad[] arr = new Actividad[con];
            for (int j = 0; j < con; j++) {
                Actividad ac = a[j];
                Actividad m = new Actividad();
                m.setUsuario(ac.getUsuario());
                m.setNombre(ac.getNombre());
                m.setDescripcion(ac.getDescripcion());
                m.setFecha(Util.convertirFecha(ac.getFechaString()));
                m.setHora(Util.convertirHora(ac.getHoraString()));
                m.setContacto(ac.getContacto());

                arr[j] = m;
            }
            int n = 0;
            Actividad[] arr1 = new Actividad[arr.length];
            for (int h = 0; h < arr.length; h++) {
                //Separamos un contacto en datos independientes
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String fecha = sdf.format(date);
                Actividad a1 = arr[h];
                if (fecha.equals(a1.getFechaString())) {
                    Actividad m = new Actividad();
                    m.setUsuario(a1.getUsuario());
                    m.setNombre(a1.getNombre());
                    m.setDescripcion(a1.getDescripcion());
                    m.setFecha(Util.convertirFecha(a1.getFechaString()));
                    m.setHora(Util.convertirHora(a1.getHoraString()));
                    m.setContacto(a1.getContacto());
                    arr1[n++] = m;
                }
            }
            return arr1;
        }
        return null;
    }

// Menú para el perfil de del usuario solo modifica la información del usuario correspondiente.
    public void inicioPersona() {
        String[] opciones = {"Modificar", "Mostrar"};
        String lista = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                "Perfil", JOptionPane.QUESTION_MESSAGE, null, opciones, null);
        try {
            switch (lista) {
                case "Modificar":
                    String ced = Util.leerTexto("Introduzca su número de cédula");
                    PersonasProyecto pp = consultarPersonaCed(ced);

                    String info = pp.getInfo();
                    String nombre = Util.leerTexto("Nombre", pp.getNombre());
                    String correo = Util.leerTexto("Correo", pp.getCorreo());
                    String cedula = pp.getCedula();
                    Date fecha = Util.leerFecha("Fecha Nacimiento", pp.getFechaNacimientoString());
                    String contraseña = Util.leerTexto("Contraseña", pp.getContraseña());
                    String usuario = Util.leerTexto("Usuario", pp.getUsuario());
                    int telefono = Util.leerInt("Teléfono", pp.getTelefono());
                    String p1 = Util.leerTexto("Pregunta 1", pp.getP1());
                    String p2 = Util.leerTexto("Pregunta 2", pp.getP2());
                    String p3 = Util.leerTexto("Pregunta 3", pp.getP3());
                    PersonasProyecto temp = new PersonasProyecto(nombre, correo, cedula,
                            fecha, contraseña, usuario, telefono, p1, p2, p3);
                    editarPersona(info, temp);
                    inicioPersona();
                    break;
                case "Mostrar":
                    PersonasProyecto[] arr = consultarPersonasUser();
                    Util.mostrar(impPersonas(arr));
                    inicioPersona();
                    break;

            }
        } catch (Exception e) {
            menuPrincipal();
        }
    }
// Menu para contactos, solo realiza funciones para los contactos. 

    public void inicioContactos(String user) {
        String[] opciones = {"Agregar", "Modificar", "Eliminar", "Mostrar"};
        String lista = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                "Contactos", JOptionPane.QUESTION_MESSAGE, null, opciones, null);
        try {
            switch (lista) {
                case "Agregar":
                    String nombre = Util.leerTexto("Nombre");
                    String usuario = usuarioLog;
                    Date fecha = Util.leerFecha("Fecha Nacimiento");
                    String correo = Util.leerTexto("Correo");
                    int telefono = Util.leerInt("Teléfono");
                    String descripcion = Util.leerTexto("Descripción");
                    Contactos contac = new Contactos(nombre, usuario, fecha, correo, telefono, descripcion);
                    registrarContacto(contac);
                    inicioContactos(user);
                    break;
                case "Modificar":
                    String nomb = Util.leerTexto("Digite el nombre de la contacto a modificar");
                    Contactos[] con = consultarContactoArr(nomb, usuarioLog);
                    int num = Util.leerInt("Contactos:\n"
                            + impContactos(con)
                            + "\nSeleccione un contacto");
                    Contactos cMo = con[num - 1];

                    String info = cMo.getInfo();
                    nombre = Util.leerTexto("Nombre", cMo.getNombre());
                    usuario = cMo.getUsuario();
                    fecha = Util.leerFecha("Fecha Nacimiento", cMo.getFechaNacimientoString());
                    correo = Util.leerTexto("Correo", cMo.getCorreo());
                    telefono = Util.leerInt("Teléfono", cMo.getTelefono());
                    descripcion = Util.leerTexto("Desscripción", cMo.getDescripcion());
                    Contactos temp = new Contactos(nombre, usuario, fecha, correo, telefono, descripcion);
                    editarContacto(info, temp);
                    inicioContactos(user);
                    break;
                case "Eliminar":
                    String nom = Util.leerTexto("Digite el nombre del contacto a eliminar");
                    Contactos[] contactos = consultarContactoArr(nom, usuarioLog);
                    num = Util.leerInt("Contactos:\n"
                            + impContactos(contactos)
                            + "\nSeleccione un contacto");
                    Util.mostrar(String.valueOf(num));
                    if (num != -1) {
                        Contactos cEli = contactos[num - 1];
                        if (Util.confirmar("¿Está seguro que desea eliminar a: " + cEli.getNombre())) {
                            eliminarContactos(cEli);
                            inicioContactos(user);
                        }
                    } else {
                        inicioContactos(user);
                    }
                    break;
                case "Mostrar":
                    Contactos[] arr = consultarContactosUser();
                    Util.mostrar(impContactos(arr));
                    inicioContactos(user);
                    break;

            }
        } catch (Exception e) {
            menuPrincipal();
        }
    }
// Menu para realizar las actividades.

    public String buscarContacto() {
        String nombre = "";
        String dato = Util.leerTexto("Nombre de contacto, Cero(0) para salir");
        if (dato.equals("0")) {
            inicioActividades();
        }
        Contactos[] c = consultarContactoArr(dato, usuarioLog);
        if (c[0] == null) {
            Util.mostrar("Contacto no encontrado");
            buscarContacto();
        } else {
            Contactos c1 = c[0];
            nombre = c1.getNombre();
        }
        return nombre;
    }

    public void asocContacto() {
        String[] opcionesCont = {"Agregar contacto", "No agregar contacto"};
        String listaCont = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                "UTN", JOptionPane.QUESTION_MESSAGE, null, opcionesCont, null);
        switch (listaCont) {
            case "Agregar contacto":
                contacto = buscarContacto();
                break;
            case "No agregar contacto":
                contacto = "0";
                break;
        }
    }

    public void guardar(String usuario, String nombre, String descripcion, Date fecha, Date hora) {
        asocContacto();
        Actividad a = new Actividad(usuario, nombre, descripcion, fecha, hora, contacto);
        registrarAtividad(a);
        inicioActividades();
    }

    public void editar(String info, String usuario, String nombre, String descripcion, Date fecha, Date hora) {
        asocContacto();
        Actividad a = new Actividad(usuario, nombre, descripcion, fecha, hora, contacto);
        editarActividad(info, a);
        inicioActividades();
    }

    public void inicioActividades() {
        String[] opciones = {"Agregar", "Modificar", "Eliminar", "Mostrar"};
        String lista = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                "Actividades", JOptionPane.QUESTION_MESSAGE, null, opciones, null);
        try {
            switch (lista) {
                case "Agregar":
                    Date sistHora = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String usuario = usuarioLog;
                    String nombre = Util.leerTexto("Nombre");
                    String descripcion = Util.leerTexto("Descripción");
                    Date fecha = Util.leerFechaHora("Fecha y Hora (31/12/2000 07:57 PM)");
                    Date hora = fecha;
                    String f = festivo(sdf.format(fecha));
                    if (f.equals("Día Festivo")) {
                        if (Util.confirmar("¿Está seguro de guardar una actividad en día festivo?")) {
                            guardar(usuario, nombre, descripcion, fecha, hora);
                        } else {
                            inicioActividades();
                        }
                    } else {
                        guardar(usuario, nombre, descripcion, fecha, hora);
                    }

                    break;
                case "Modificar":
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                    String nomb = Util.leerTexto("Digite el nombre de la Actividad a modificar");
                    Actividad[] actividades = consultarActividadesArr(nomb, usuarioLog);
                    int num = Util.leerInt("Actividad:\n"
                            + impActividades(actividades)
                            + "\nSeleccione una actividad");
                    Actividad AMo = actividades[num - 1];

                    String info = AMo.getInfo();
                    usuario = AMo.getUsuario();
                    nombre = Util.leerTexto("Nombre", AMo.getNombre());
                    descripcion = Util.leerTexto("Desscripción", AMo.getDescripcion());
                    fecha = Util.leerFechaHora("Fecha y Hora (31/12/2000 07:57 PM)", AMo.getFechaString());
                    hora = fecha;
                     f = festivo(sdf1.format(fecha));
                    if (f.equals("Día Festivo")) {
                        if (Util.confirmar("¿Está seguro de guardar una actividad en día festivo?")) {
                            editar(info,usuario, nombre, descripcion, fecha, hora);
                        } else {
                            inicioActividades();
                        }
                    } else {
                        editar(info,usuario, nombre, descripcion, fecha, hora);
                    }
                    break;
                case "Eliminar":
                    String nom = Util.leerTexto("Digite el nombre de la actividad a eliminar");
                    Actividad[] actividads = consultarActividadesArr(nom, usuarioLog);
                    num = Util.leerInt("Actividad:\n"
                            + impActividades(actividads)
                            + "\nSeleccione la actividad");
                    Util.mostrar(String.valueOf(num));
                    if (num != -1) {
                        Actividad AEli = actividads[num - 1];
                        if (Util.confirmar("¿Está seguro que desea eliminar a: " + AEli.getNombre())) {
                            eliminarActividad(AEli);
                            inicioActividades();
                        }
                    } else {
                        inicioActividades();
                    }
                    break;
                case "Mostrar":
                    String[] opci = {"Todas", "Semanal", "Mensual"};
                    String listaM = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                            "Mostrar Actividades", JOptionPane.QUESTION_MESSAGE, null, opci, null);
                    switch (listaM) {
                        case "Todas":
                            Actividad[] arr = consultarActividadesUser();
                            Util.mostrar(impActividades(arr));
                            inicioActividades();
                            break;
                        case "Semanal":
                            Date d = calcHora(7);
                            Actividad[] ac = notificacionesArr(d);
                            if (ac[0] == null) {
                                Util.mostrar("No hay actividades en este periodo");
                                inicioActividades();
                            } else {
                                Util.mostrar(impActividadesFestivos(ac));
                                inicioActividades();
                            }
                            break;
                        case "Mensual":
                            d = calcHora(30);
                            Actividad[] ac1 = notificacionesArr(d);
                            if (ac1[0] == null) {
                                Util.mostrar("No hay actividades en este periodo");
                                inicioActividades();
                            } else {
                                Util.mostrar(impActividadesFestivos(ac1));
                                inicioActividades();
                            }
                            break;
                    }
                    break;

            }
        } catch (Exception e) {
            menuPrincipal();
        }
    }

    public void inicioFestivos() {
        String[] opciones = {"Agregar", "Modificar", "Eliminar", "Mostrar"};
        String lista = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                "Festivos", JOptionPane.QUESTION_MESSAGE, null, opciones, null);
        try {
            switch (lista) {
                case "Agregar":
                    String usuario = usuarioLog;
                    String nombre = Util.leerTexto("Nombre");
                    Date fecha = Util.leerFecha("Fecha");
                    DiaFestivo df = new DiaFestivo(usuario, nombre, fecha);
                    registrarDiaFestivo(df);
                    inicioFestivos();
                    break;
                case "Modificar":
                    String nomb = Util.leerTexto("Digite el nombre del dia festivo a modificar");
                    DiaFestivo[] festi = consultarDiaFestivoArr(nomb, usuarioLog);
                    int a = Util.leerInt("Dia Festivo:\n"
                            + impDiaFestivo(festi)
                            + "\nSeleccione un dia festivo");
                    DiaFestivo FMo = festi[a - 1];

                    String info = FMo.getInfo();
                    usuario = FMo.getUsuario();
                    nombre = Util.leerTexto("Nombre", FMo.getNombre());
                    fecha = Util.leerFecha("Fecha Nacimiento", FMo.getFechaString());

                    DiaFestivo temp = new DiaFestivo(usuario, nombre, fecha);
                    editarDiaFestivo(info, temp);
                    inicioFestivos();
                    break;
                case "Eliminar":
                    String nom = Util.leerTexto("Digite el nombre del dia festivo a eliminar");
                    DiaFestivo[] festivo = consultarDiaFestivoArr(nom, usuarioLog);
                    a = Util.leerInt("Dia Festivo:\n"
                            + impDiaFestivo(festivo)
                            + "\nSeleccione un dia festivo");

                    if (a != -1) {
                        DiaFestivo FEli = festivo[a - 1];
                        if (Util.confirmar("¿Está seguro que desea eliminar a: " + FEli.getNombre())) {
                            eliminarDiaFestivo(FEli);
                            inicioFestivos();
                        }
                    } else {
                        inicioFestivos();
                    }
                    break;
                case "Mostrar":
                    DiaFestivo[] di = consultarDiaFestivoUser();
                    Util.mostrar(impDiaFestivo(di));
                    inicioFestivos();
                    break;

            }
        } catch (Exception e) {
            menuPrincipal();
        }
    }

    // Menu principal el cual nos mostrara las opciones mas generales.
    public void menuPrincipal() {
        String[] opciones = {"Perfil", "Mantenimiento Contactos", "Mantenimiento Actividades", "Mantenimiento Festivos"};
        String lista = (String) JOptionPane.showInputDialog(null, "Escoja la opción que desea",
                "UTN", JOptionPane.QUESTION_MESSAGE, null, opciones, null);
        try {
            switch (lista) {
                case "Perfil":
                    inicioPersona();
                    break;
                case "Mantenimiento Contactos":
                    inicioContactos(usuarioLog);
                    break;
                case "Mantenimiento Actividades":
                    inicioActividades();
                    break;
                case "Mantenimiento Festivos":
                    inicioFestivos();
                    break;
            }
        } catch (Exception e) {

        }
    }

// Metodos para editar.
    public void editarPersona(String info, PersonasProyecto temp) {
        String datos = ma.leer(RUTA_PERS);
        datos = datos.replaceAll(info, temp.getInfo()).trim();
        ma.escribir(RUTA_PERS, datos, false);
    }

    public void editarContacto(String info, Contactos temp) {
        String datos = ma.leer(RUTA_CONT);
        datos = datos.replaceAll(info, temp.getInfo()).trim();
        ma.escribir(RUTA_CONT, datos, false);
    }

    public void editarDiaFestivo(String info, DiaFestivo temp) {
        String datos = ma.leer(RUTA_FEST);
        datos = datos.replaceAll(info, temp.getInfo()).trim();
        ma.escribir(RUTA_FEST, datos, false);
    }

    public void editarActividad(String info, Actividad temp) {
        String datos = ma.leer(RUTA_ACTI);
        datos = datos.replaceAll(info, temp.getInfo()).trim();
        ma.escribir(RUTA_ACTI, datos, false);
    }

    // Metodos para consultar si el dato solicitado existe o no.
    public PersonasProyecto[] consultarPersonas() {
        //Separando el texto del archivo en lineas que representan una persona
        String[] personas = ma.leer(RUTA_PERS).split("\n");

        //Creamos un arreglo de personas del mismo tamaño que las lineas del archivo
        PersonasProyecto[] arr = new PersonasProyecto[personas.length];

        //Recorremos linea por linea para poder separar los datos de cada persona
        for (int i = 0; i < personas.length; i++) {
            //Separamos una persona en datos independientes
            String[] datos = personas[i].split(",");

            //Creamos una persona con los datos 
            PersonasProyecto m = new PersonasProyecto();
            m.setNombre(datos[0]);
            m.setCorreo(datos[1]);
            m.setCedula(datos[2]);
            m.setFechaNacimiento(Util.convertirFecha(datos[3]));
            m.setContraseña(datos[4]);
            m.setUsuario(datos[5]);
            m.setTelefono(Integer.parseInt(datos[6]));
            m.setP1(datos[7]);
            m.setP2(datos[8]);
            m.setP3(datos[9]);

            //Agregamos una persona al arreglo
            arr[i] = m;
        }
        return arr;
    }

    /**
     * 
     * @param usuario
     * @return 
     */
    public PersonasProyecto consultarPersona(String usuario) {
        String[] personas = ma.leer(RUTA_PERS).split("\n");
        int i = 0;
        for (String linea : personas) {
            String[] datos = linea.split(",");
            if (usuario.equals(datos[5])) {
                PersonasProyecto m = new PersonasProyecto();
                m.setNombre(datos[0]);
                m.setCorreo(datos[1]);
                m.setCedula(datos[2]);
                m.setFechaNacimiento(Util.convertirFecha(datos[3]));
                m.setContraseña(datos[4]);
                m.setUsuario(datos[5]);
                m.setTelefono(Integer.parseInt(datos[6]));
                m.setP1(datos[7]);
                m.setP2(datos[8]);
                m.setP3(datos[9]);
                return m;
            }
        }
        return null;
    }

    public PersonasProyecto consultarPersonaCed(String ced) {
        String[] personas = ma.leer(RUTA_PERS).split("\n");
        int i = 0;
        for (String linea : personas) {
            String[] datos = linea.split(",");
            if (ced.equals(datos[2])) {
                PersonasProyecto m = new PersonasProyecto();
                m.setNombre(datos[0]);
                m.setCorreo(datos[1]);
                m.setCedula(datos[2]);
                m.setFechaNacimiento(Util.convertirFecha(datos[3]));
                m.setContraseña(datos[4]);
                m.setUsuario(datos[5]);
                m.setTelefono(Integer.parseInt(datos[6]));
                m.setP1(datos[7]);
                m.setP2(datos[8]);
                m.setP3(datos[9]);
                return m;
            }
        }
        return null;
    }

    public DiaFestivo[] consultarFestivos() {
        //Separando el texto del archivo en lineas que representan un dia
        String[] dias = ma.leer(RUTA_FEST).split("\n");

        //Creamos un arreglo de dias del mismo tamaño que las lineas del archivo
        DiaFestivo[] arr = new DiaFestivo[dias.length];

        //Recorremos linea por linea para poder separar los datos de cada dia
        for (int i = 0; i < dias.length; i++) {
            //Separamos un dia en datos independientes
            String[] datos = dias[i].split(",");

            //Creamos un dia con los datos 
            DiaFestivo m = new DiaFestivo();
            m.setUsuario(datos[0]);
            m.setNombre(datos[1]);
            m.setFecha(Util.convertirFecha(datos[2]));

            //Agregamos un dia al arreglo
            arr[i] = m;
        }
        return arr;
    }

    public Contactos consultarContacto(String dato) {
        String[] contactos = ma.leer(RUTA_CONT).split("\n");
        int i = 0;
        for (String linea : contactos) {
            String[] datos = linea.split(",");
            if (dato.equals(datos[0])) {
                Contactos m = new Contactos();
                m.setNombre(datos[0]);
                m.setCorreo(datos[1]);
                m.setUsuario(datos[2]);
                m.setFechaNacimiento(Util.convertirFecha(datos[3]));
                m.setTelefono(Integer.parseInt(datos[4]));
                m.setDescripcion(datos[5]);
                return m;
            }
        }
        return null;
    }

    public Contactos[] consultarContactoArr(String dato, String usuario) {
        String[] contactos = ma.leer(RUTA_CONT).split("\n");
        Contactos[] arr = new Contactos[contactos.length];
        int i = 0;
        for (String linea : contactos) {
            //Separamos un contacto en datos independientes
            String[] datos = linea.split(",");
            if (dato.equals(datos[0]) && usuario.equals(datos[1])) {
                Contactos m = new Contactos();
                m.setNombre(datos[0]);
                m.setUsuario(datos[1]);
                m.setFechaNacimiento(Util.convertirFecha(datos[2]));
                m.setCorreo(datos[3]);
                m.setTelefono(Integer.parseInt(datos[4]));
                m.setDescripcion(datos[5]);
                arr[i++] = m;
            }
        }
        return arr;
    }

    public DiaFestivo[] consultarDiaFestivoArr(String dato, String usuario) {
        String[] festivo = ma.leer(RUTA_FEST).split("\n");
        DiaFestivo[] arr = new DiaFestivo[festivo.length];
        int i = 0;
        for (String linea : festivo) {
            String[] datos = linea.split(",");
            if (dato.equals(datos[1]) && usuario.equals(datos[0])) {
                DiaFestivo m = new DiaFestivo();
                m.setUsuario(datos[0]);
                m.setNombre(datos[1]);
                m.setFecha(Util.convertirFecha(datos[2]));
                arr[i++] = m;
            }
        }
        return arr;
    }

    public Actividad[] consultarActividadesArr(String dato, String usuario) {
        String[] activi = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arr = new Actividad[activi.length];
        int i = 0;
        for (String linea : activi) {
            String[] datos = linea.split(",");
            if (dato.equals(datos[1]) && usuario.equals(datos[0])) {
                Actividad m = new Actividad();
                m.setUsuario(datos[0]);
                m.setNombre(datos[1]);
                m.setDescripcion(datos[2]);
                m.setFecha(Util.convertirFecha(datos[3]));
                m.setHora(Util.convertirFecha(datos[4]));
                m.setContacto(datos[5]);

                arr[i++] = m;
            }
        }
        return arr;
    }

    public Contactos[] consultarContactosUser() {
        String[] contactos = ma.leer(RUTA_CONT).split("\n");
        Contactos[] arr = new Contactos[contactos.length];
        int i = 0;
        for (String linea : contactos) {
            //Separamos un contacto en datos independientes
            String[] datos = linea.split(",");
            if (usuarioLog.equals(datos[1])) {
                Contactos m = new Contactos();
                m.setNombre(datos[0]);
                m.setUsuario(datos[1]);
                m.setFechaNacimiento(Util.convertirFecha(datos[2]));
                m.setCorreo(datos[3]);
                m.setTelefono(Integer.parseInt(datos[4]));
                m.setDescripcion(datos[5]);
                arr[i++] = m;
            }
        }
        return arr;
    }

    public PersonasProyecto[] consultarPersonasUser() {
        String[] persona = ma.leer(RUTA_PERS).split("\n");
        PersonasProyecto[] arr = new PersonasProyecto[persona.length];
        int i = 0;
        for (String linea : persona) {
            String[] datos = linea.split(",");
            if (usuarioLog.equals(datos[2])) {
                PersonasProyecto m = new PersonasProyecto();
                m.setNombre(datos[0]);
                m.setCorreo(datos[1]);
                m.setCedula(datos[2]);
                m.setFechaNacimiento(Util.convertirFecha(datos[3]));
                m.setContraseña(datos[4]);
                m.setUsuario(datos[5]);
                m.setTelefono(Integer.parseInt(datos[6]));
                m.setP1(datos[7]);
                m.setP2(datos[8]);
                m.setP3(datos[9]);
                arr[i++] = m;
            }
        }
        return arr;
    }

    public Actividad[] consultarActividadesUser() {
        String[] actividades = ma.leer(RUTA_ACTI).split("\n");
        Actividad[] arr = new Actividad[actividades.length];
        int i = 0;
        for (String linea : actividades) {
            String[] datos = linea.split(",");
            if (usuarioLog.equals(datos[0])) {
                Actividad m = new Actividad();
                m.setUsuario(datos[0]);
                m.setNombre(datos[1]);
                m.setDescripcion(datos[2]);
                m.setFecha(Util.convertirFecha(datos[3]));
                m.setHora(Util.convertirHora(datos[4]));
                m.setContacto(datos[5]);

                arr[i++] = m;
            }
        }
        return arr;
    }

    public DiaFestivo[] consultarDiaFestivoUser() {
        String[] festivo = ma.leer(RUTA_FEST).split("\n");
        DiaFestivo[] arr = new DiaFestivo[festivo.length];
        int i = 0;
        for (String linea : festivo) {
            String[] datos = linea.split(",");
            if (usuarioLog.equals(datos[0])) {
                DiaFestivo m = new DiaFestivo();
                m.setUsuario(datos[0]);
                m.setNombre(datos[1]);
                m.setFecha(Util.convertirFecha(datos[2]));

                arr[i++] = m;
            }
        }
        return arr;
    }

    /**
     * 
     * @return 
     */
    public Contactos[] consultarContactos() {
        String[] contactos = ma.leer(RUTA_CONT).split("\n");
        Contactos[] arr = new Contactos[contactos.length];
        for (int i = 0; i < contactos.length; i++) {
            String[] datos = contactos[i].split(",");
            Contactos m = new Contactos();
            m.setNombre(datos[0]);
            m.setUsuario(datos[1]);
            m.setFechaNacimiento(Util.convertirFecha(datos[2]));
            m.setCorreo(datos[3]);
            m.setTelefono(Integer.parseInt(datos[4]));
            m.setDescripcion(datos[5]);
            arr[i] = m;
        }
        return arr;
    }

// Con este metodo creamos una nueva contraseña aleatoria para que el usuario pueda
//  acceder a su cuenta, esto es en caso de olvidar la actual. 
    public String contraAleatoria(PersonasProyecto usuario) {
        String contra = "";
        for (int i = 0; i < 4; i++) {
            contra += (int) (Math.random() * 8) + 1;
        }
        String info = usuario.getInfo();
        String nombre = usuario.getNombre();
        String correo = usuario.getCorreo();
        String cedula = usuario.getCedula();
        Date fechaNacimiento = usuario.getFechaNacimiento();
        String contraseña = (String.valueOf(contra));
        String usuari = usuario.getUsuario();
        int telefono = usuario.getTelefono();
        String p1 = usuario.getP1();
        String p2 = usuario.getP2();
        String p3 = usuario.getP3();
        PersonasProyecto temp = new PersonasProyecto(nombre, correo, cedula,
                fechaNacimiento, contraseña, usuari, telefono, p1, p2, p3);
        editarPersona(info, temp);
        return contraseña;
    }
// Preguntas de seguridad para recuperar la contraseña.

    public void olvidoContrasenna(PersonasProyecto usuario) {
        String[] options = {"SI", "NO"};
        int prueba = JOptionPane.showOptionDialog(null, "Desea recuperar Contraseña", "Agenda UTN",
                JOptionPane.WARNING_MESSAGE, JOptionPane.HEIGHT, null, options, null);
        System.out.println(prueba);
        switch (prueba) {
            case 0:
                String p1 = Util.leerTexto("¿Color Favorito?");
                String p2 = Util.leerTexto("¿Personaje Favorito?");
                String p3 = Util.leerTexto("¿Deporte Favorito?");
                if (p1.equals(usuario.getP1()) && p2.equals(usuario.getP2()) && p3.equals(usuario.getP3())) {
                    String contraseña = contraAleatoria(usuario);
                    Util.mostrar(contraseña);
                } else {
                    Util.mostrar("Respuestas Erroneas!!");
                }
                break;
            case 1:
            case -1:
                break;
        }
    }
//Menú inicial, este menú es para iniciar seción de su cuenta o para crear una cuenta en caso de que la tenga. 

    public void inicio() {
        String[] options = {"Registrarse", "Iniciar Seción"};
        APP:
        while (true) {
            int prueba = JOptionPane.showOptionDialog(null, "Es necesario que seleccione una opción", "Agenda UTN",
                    JOptionPane.WARNING_MESSAGE, JOptionPane.HEIGHT, null, options, null);
            switch (prueba) {
                case 0:
                    //Registro de personas
                    String nombre = Util.leerTexto("Nombre Completo");
                    String correo = Util.leerTexto("Correo");
                    String cedula = Util.leerTexto("Cédula");
                    Date fecha = Util.leerFecha("Fecha Nacimiento");
                    String contraseña = Util.leerTexto("Contraseña");
                    String usuario = Util.leerTexto("Usuario");
                    int telefono = Util.leerInt("Teléfono");
                    String p1 = Util.leerTexto("¿Color Favorito?");
                    String p2 = Util.leerTexto("¿Personaje Favorito?");
                    String p3 = Util.leerTexto("¿Deporte Favorito?");

                    PersonasProyecto temp = new PersonasProyecto(nombre, correo, cedula,
                            fecha, contraseña, usuario, telefono, p1, p2, p3);
                    registrarPersona(temp);
                    break;
                case 1:
                    //inicio seción
                    String user = Util.leerTexto("Usuario");
                    String pass = Util.leerTexto("Contraseña");
                    PersonasProyecto u = consultarPersona(user);
                    if (u != null && pass.equals(u.getContraseña())) {
                        //inicioPersona();
                        usuarioLog = u.getCedula();
                        //inicioContactos(u.getUsuario());
                        //inicioActividades();
                        Date date = new Date();
                        Actividad[] ac = notificacionesArr(date);
                        if (ac[0] == null) {
                            menuPrincipal();
                        } else {
                            Util.mostrar(impActividadesFestivos(ac));
                            menuPrincipal();
                        }
                    } else if (u != null) {
                        olvidoContrasenna(u);
                    } else {
                        Util.mostrar("Usuario o contraseña incorrectos!!");
                    }
                    break;
                case -1:
                    break APP;
            }
        }

    }
    //Metodo para eliminar datos.

    public void eliminarContactos(Contactos contacto) {
        String datos = ma.leer(RUTA_CONT);
        datos = datos.replaceAll(contacto.getInfo() + "\n", "").trim();
        ma.escribir(RUTA_CONT, datos, false);
    }

    public void eliminarDiaFestivo(DiaFestivo festivo) {
        String datos = ma.leer(RUTA_FEST);
        datos = datos.replaceAll(festivo.getInfo() + "\n", "").trim();
        ma.escribir(RUTA_FEST, datos, false);
    }

    public void eliminarActividad(Actividad activi) {
        String datos = ma.leer(RUTA_ACTI);
        datos = datos.replaceAll(activi.getInfo() + "\n", "").trim();
        ma.escribir(RUTA_ACTI, datos, false);
    }

    //Esto sirve para mostrar datos,y asi poder elegir al que nececitamos.
    public String impContactos(Contactos[] contactos) {
        String msj = "";
        String formato = "%d. %s, %s , %s, %s,%s\n";
        for (int i = 0; i < contactos.length; i++) {
            Contactos m = contactos[i];
            if (m == null) {
                return msj;
            }
            msj += String.format(formato, (i + 1),
                    m.getNombre(), m.getCorreo(), m.getFechaNacimientoString(),
                    m.getTelefono(), m.getDescripcion());
        }
        return msj;
    }

    public String impActividades(Actividad[] actividads) {
        String msj = "";
        String formato = "%d. %s, %s, %s, %s, %s\n";
        for (int i = 0; i < actividads.length; i++) {
            Actividad m = actividads[i];
            if (m == null) {
                return msj;
            }
            msj += String.format(formato, (i + 1),
                    m.getNombre(), m.getDescripcion(), m.getFechaString(), m.getHoraString(),
                    m.getContacto());
        }
        return msj;
    }

    public String impDiaFestivo(DiaFestivo[] festivo) {
        String msj = "";
        String formato = "%d. %s, %s, %s\n";
        for (int i = 0; i < festivo.length; i++) {
            DiaFestivo m = festivo[i];
            if (m == null) {
                return msj;
            }
            msj += String.format(formato, (i + 1),
                    m.getUsuario(), m.getNombre(), m.getFechaString());

        }
        return msj;
    }

    public String impPersonas(PersonasProyecto[] persona) {
        String msj = "";
        String formato = "%d. %s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n";
        for (int i = 0; i < persona.length; i++) {
            PersonasProyecto m = persona[i];
            if (m == null) {
                return msj;
            }
            msj += String.format(formato, (i + 1),
                    m.getNombre(), m.getCorreo(), m.getCedula(), m.getFechaNacimientoString(),
                    m.getContraseña(), m.getUsuario(), m.getTelefono(), m.getP1(), m.getP2(), m.getP3());
        }
        return msj;
    }

    public String impActividadesFestivos(Actividad[] actividads) {
        int tel = 0;
        String correo = "";
        String nacim = "";
        String msj = "";
        String c = "";
        String formato = "";
        for (int i = 0; i < actividads.length; i++) {
            Actividad m = actividads[i];
            if (m == null) {
                return msj;
            }
            if (m.getContacto().equals("0")) {
                c = "N/A";
                formato = "%d. %s, %s, %s, %s, %s, %s\n";
                msj += String.format(formato, (i + 1), festivo(m.getFechaString()),
                        m.getNombre(), m.getDescripcion(), m.getFechaString(), m.getHoraString(),
                        c);
            } else {
                c = m.getContacto();
                Contactos[] con = consultarContactoArr(m.getContacto(), usuarioLog);
                for (int j = 0; j < con.length; j++) {
                    Contactos c1 = con[j];
                    if (m.getContacto().equals(c1.getNombre())) {
                        tel = c1.getTelefono();
                        correo = c1.getCorreo();
                        nacim = c1.getFechaNacimientoString();
                        break;
                    }
                }
                formato = "%d. %s, %s, %s, %s, %s, %s,%d años, tel: %d, email: %s, nació: %s\n";
                msj += String.format(formato, (i + 1), festivo(m.getFechaString()),
                        m.getNombre(), m.getDescripcion(), m.getFechaString(), m.getHoraString(),
                        c, edad(m.getContacto()), tel, correo, nacim);
            }

        }
        return msj;
    }
//Este metodo es para que la hora sea igual a la de la compu
    public void Hora() {
        Date sistHora = new Date();
        String pmAm = "hh:mm a";
        SimpleDateFormat format = new SimpleDateFormat(pmAm);
        Calendar hoy = Calendar.getInstance();
        String aa = "";
        aa = format.format(sistHora);
        try {
            ho = format.parse(aa);
        } catch (ParseException ex) {
            Logger.getLogger(LogicaProyecto.class.getName()).log(Level.SEVERE, null, ex);
        }
       // System.out.println(aa);
        //JOptionPane.showMessageDialog(null, String.format(format.format(sistHora), hoy));
    }
// Este metodo es para que la fecha sea igual a la de la compu
    public void Fecha() {
        Date sistFecha = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd-MMM-YYYY");
        //JOptionPane.showMessageDialog(null, formato.format(sistFecha));
    }
}
