package randallarceagenda;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author randall
 */
public class Actividad {
    private String usuario;
    private String nombre;
    private String descripcion;
    private Date fecha;
    private Date hora;
    private String contacto;

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }
    
    
    public Actividad(){}

    public Actividad(String usuario, String nombre, String descripcion, Date fecha, Date hora, String contacto) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.hora = hora;
        this.contacto = contacto;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }
    
       public String getFechaString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fecha);
    }
       
            public String getHoraString() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        return sdf.format(hora);
    }
    
    
    public String getInfo() {
        String temp = "%s,%s,%s,%s,%s,%s";
        return String.format(temp,usuario,nombre,descripcion,getFechaString(),getHoraString(),contacto);
    }
    
}
