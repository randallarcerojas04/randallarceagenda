package randallarceagenda;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author randall
 */
public class DiaFestivo {
    private String usuario;
    private String nombre;
    private Date fecha;
    
    public DiaFestivo(){}

    public DiaFestivo(String usuario, String nombre, Date fecha) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
       public String getFechaString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fecha);
    }

   
    public String getInfo() {
        String temp = "%s,%s,%s";
        return String.format(temp,usuario,nombre,getFechaString());
    }
    
}
