package randallarceagenda;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author Randall
 */
public class PersonasProyecto {
    private String nombre;
    private String correo;
    private String cedula;
    private Date fechaNacimiento;
    private String contraseña;
    private String usuario;
    private int telefono;
    private String p1;
    private String p2;
    private String p3;
    
    public PersonasProyecto(){}

    public PersonasProyecto(String nombre, String correo, String cedula, Date fechaNacimiento, String contraseña, String usuario, int telefono, String p1, String p2, String p3) {
        this.nombre = nombre;
        this.correo = correo;
        this.cedula = cedula;
        this.fechaNacimiento = fechaNacimiento;
        this.contraseña = contraseña;
        this.usuario = usuario;
        this.telefono = telefono;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public String getP2() {
        return p2;
    }

    public void setP2(String p2) {
        this.p2 = p2;
    }

    public String getP3() {
        return p3;
    }

    public void setP3(String p3) {
        this.p3 = p3;
    }

     

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    
    public String getFechaNacimientoString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNacimiento);
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    
     public String getInfo() {
        String temp = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s";
        return String.format(temp, nombre, correo, cedula, getFechaNacimientoString(), contraseña, usuario, telefono,p1,p2,p3);
    }
     
         @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", correo=" + correo + ", cedula=" + cedula + ", fechaNacimiento=" 
                + fechaNacimiento + ", contraseña=" + contraseña + ", usuario=" + usuario + ", telefono=" + telefono +", respuesta1=" + p1 + ", respuesta2=" + p2 + ", respuesta3=" + p3 + '}';
    }

}