package randallarceagenda;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author randall
 */


public class Contactos {
    private String nombre;
    private String usuario; 
    private Date fechaNacimiento;
    private String correo;
    private int telefono;
    private String descripcion;
    
    public Contactos(){}
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }
    
    public String getFechaNacimientoString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNacimiento);
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    

    public Contactos(String nombre, String usuario, Date fechaNacimiento, String correo, int telefono, String descripcion) {
        this.nombre = nombre;
        this.usuario = usuario;
        this.fechaNacimiento = fechaNacimiento;
        this.correo = correo;
        this.telefono = telefono;
        this.descripcion = descripcion;
    }

   
    public String getInfo() {
        String temp = "%s,%s,%s,%s,%s,%s";
        return String.format(temp, nombre, usuario, getFechaNacimientoString(), correo, telefono, descripcion);
    }
    
}
